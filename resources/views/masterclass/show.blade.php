@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page tabcontent" id="main-page-news">
  <div class="link">
    <span><a href="/">Главная</a> / </span><span><a href="/masterclass">Мастер Класс</a> / </span><span><a href="{{ $masterclass->name }}">{{$masterclass->name}}</a></span>
  </div>
    <div class="card  col-sm-12 shadow big-card-news">
      <div class="title">
        <h1>{{ $masterclass->name}}</h1>
      </div>
            <div class="masterclass-image" style="text-align:center;">
              <div><img src=" {{ Voyager::image( $masterclass->image) }}" style="width:85%"></div>
            </div>
            <div class="card-content" style="color: #000;">
              <?php 
                echo $masterclass->description
              ?>    
            </div>
            </div>
                </div>
</div>


@endsection
