@extends('layouts.header')

@section('content')
<div class="contains text-center">
  <h1>GET LINK</h1>

        <form action="{{ route('send-confirmation-email', $user) }}" method="post">
              {{ csrf_field() }}
              <input type="hidden" value="{{ $user->email }}" name="email">
              <input type="submit" class="btn btn-primary" value="Send!">
          </form>
  {{ $user->email }}
</div>
@endsection
