@extends('layouts.header')

@section('content')
              <div class="col-sm-12 content-page tabcontent" id="main-page-news">
                <div class="link">
                  <span><a href="/">Главная</a> / </span><span><a href="/">Новости</a> / </span><span><a href="{{ $news->title }}">{{$news->title}}</a></span>
                </div>
                <div class="card  col-sm-12 shadow big-card-news">
                  <div class="title">
                    <h1>{{ $news->title}}</h1>
                  </div>
                  <div id="jssor_1">
                      <!-- Loading Screen -->
                      <div data-u="loading" class="jssorl-009-spin" style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                          <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;" src="images/spin.svg">
                      </div>
                      <div class="imagesofslide" data-u="slides">
                        @if(is_null($news->gallery))
                        <div><img data-u="{{ $news->image }}" src=" {{Voyager::image($news->image )}}" style="width:100%"></div>
                        @else
                        @foreach (json_decode($news->gallery) as $slide)
                        <div><img data-u="{{ $slide }}" src=" {{Voyager::image($slide) }}" style="width:100%"></div>
                        @endforeach
                        @endif
                      </div>
                      <!-- Bullet Navigator -->
                      <div data-u="navigator" class="jssorb031" style="position:absolute;bottom:12px;right:12px;" data-autocenter="2" data-scale="0.5" data-scale-right="0.75">
                          <div data-u="prototype" class="i" style="width:12px;height:12px;">
                              <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                                  <circle class="b" cx="8000" cy="8000" r="5800"></circle>
                              </svg>
                          </div>
                      </div>
                  </div>
                  <div class="card-content">
                    <p>{{$news->description}}</p>
                  </div>
                </div>


<script type="text/javascript">
  $("#defaultOpen").addClass("active");
jssor_1_slider_init = function() {

var jssor_1_options = {
$AutoPlay: 1,
$DragOrientation: 2,
$PlayOrientation: 2,
$BulletNavigatorOptions: {
$Class: $JssorBulletNavigator$,
$Orientation: 2
}
};

var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

/*#region responsive code begin*/

var MAX_WIDTH = 980;

function ScaleSlider() {
var containerElement = jssor_1_slider.$Elmt.parentNode;
var containerWidth = containerElement.clientWidth;

if (containerWidth) {

var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

jssor_1_slider.$ScaleWidth(expectedWidth);
}
else {
window.setTimeout(ScaleSlider, 30);
}
}

ScaleSlider();

$Jssor$.$AddEvent(window, "load", ScaleSlider);
$Jssor$.$AddEvent(window, "resize", ScaleSlider);
$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
/*#endregion responsive code end*/
};
</script>

<script type="text/javascript">jssor_1_slider_init();
</script>
</div>


@endsection
