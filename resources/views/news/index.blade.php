@extends('layouts.header')

@section('content')


              <div class="col-sm-12 content-page" id="page-news">
                <div class="title">
                  <h1>Новости</h1>
                </div>
                <div class="row">
                    @foreach ($news as $newsItem)
            <div class="col-sm-4">


              <div class="card shadow card-news">
                <div class="card-img">
                  <img src="{{ Voyager::image($newsItem->image) }}" alt="">
                </div>
                <div class="card-content">
                  <a href="/news/{{ $newsItem->id }}/{{ $newsItem->title }}">
                  <div class="card-title">
                    <h3>{{ $newsItem->title }}</h3>
                  </div>
                  <div class="card-date">
                    <p>{{ \Carbon\Carbon::parse($newsItem->created_at)->format('d.m.y') }}</p>
                  </div>
                  </a>
                </div>
              </div>


            </div>
        @endforeach

            </div>
            </div>


            <script type="text/javascript">
              $("#defaultOpen").addClass("active");

            </script>

@endsection
