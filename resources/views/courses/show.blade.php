@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page tabcontent" id="page-kursy">
  <div class="link">
	  <span><a href="/">Главная</a> / </span><span><a href="/courses">Курсы</a> / </span><span>{{$courses->title}}</span>
  </div>
  <div class="title">
    <h1>{{$courses->title}}</h1>
  </div>
<div class="card-course-main col-sm-12">
  <div class="row">
    <div class="video col-sm-6">
      <img src="{{Voyager::image($courses->image)}}" alt="{{$courses->title}}">
    </div>
    <div class="courses-description col-sm-6">
      <p><?php
          echo $courses->dulldescription
      ?></p>
      @if($purchase)
      <a class="btn" href="/page/{{ $courses->id }}">Начать обучениу курса</a>
        @else
        <div class="video-button">
          <button class="btn btn-primary" type="button" name="button" data-toggle="modal" data-target="#myModal">Купить</button>
        </div>
        @endif
    </div>
  </div>
  <div class="course-comments">
    <ul class="list-group">
    @foreach($courses->comments as $comment)
    <li class="list-group-item">
      <strong class="users-name">
        {{ $comment->user->name ?? '' }}
    </strong>
     <p class="comment-body">{{$comment->body}}</p>
     <p  class="date-comments">
       {{ \Carbon\Carbon::parse($comment->created_at)->format('d.m.y') }}
     </p>
      </li>
    @endforeach
  </ul>
  <!-- Add comments -->
  <hr>
  <div class="cards">
    <div class="card-blocks">
      <form method="POST" action="/course/{{ $courses->id }}/comments" >
        {{csrf_field()}}
        <div class="form-group add-comment">
          <textarea name="body" class="form-control" placeholder="Ваш комментарий...">
          </textarea>
          <button type="submit" class="btn-send-comment"><img src="{{asset('images/send-button.png')}}" alt="send" width="100%"></button>
        </div>
      </form>
    </div>
  </div>
  </div>
  <div class="container">
    <div class="modal fade" id="myModal">
      <div class="modal-dialog modal-dialog-top">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title"></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <p>Курс стоит {{$courses->price}} тг</p>
            <p>Вы действительно хотите купить?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" id="buy-btn">Да</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
          </div>

        </div>
      </div>
    </div>

  </div>

  </div>

</div>

<form action="{{route('buy', $courses)}}" method="post" id="buy">
  @csrf
</form>
@if(!empty(Session::get('error')) && Session::get('error') == 2)
<script>
$(function() {
    $('#modal-info-changed').modal('show');
});
</script>
@endif
@if(!empty(Session::get('success')) && Session::get('success') == 1)
<script>
$(function() {
    $('#modal-buy').modal('show');
});
</script>
@endif
<script type="text/javascript">
  $('#buy-btn').click(function(){
    $('#buy').submit();
    $('#myModal').modal('hide');
  });
</script>
</div>

<div class="modal fade" id="modal-info-changed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>На вашем счёте недостаточно средств</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-success btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-buy" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Поздравляем! Курс {{$courses->title}} был успешно куплен</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-success btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
@endsection
