@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page" id="page-kursy">
  <div class="title">
    <h1>Курсы</h1>
  </div>
  <div class="row">
    @foreach($categories as $category)
    <div class="col-sm-4">
      <div class="card shadow card-news">
      <a href="category/{{$category->id}}">
        <div class="card-img">
        </div>
        <div class="card-content">
          <div class="card-title">
            <h3>
              {{ $category->name }}
            </h3> 
          </div>
          <div class="card-date">
            <p>
            </p>
          </div>
          </a>
        </div>
      </a>
      </div>
    </div>
      @endforeach

    </div>
  </div>
</div>
<script type="text/javascript">
  $("#courseslink").addClass("active");
</script>
@endsection
