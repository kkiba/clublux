@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page tabcontent" id="page-order">
  <?php $count = 0; ?>
  @foreach($videos as $video)
<div class="col-sm-6 order-videos">
  <div class="open-video">
    <a id="myBtn" class="myBtn" data-toggle="modal" data-target="#myModal<?php echo $count; ?>">
        <div>
          <img src="{{Voyager::image($video->image)}}"/ width="50%" height="35%">
        </div>
        <div>
          <span class="video-title">{{$video->title}}</span>
        </div>
    </a>
  </div>
  <div id="myModal<?php echo $count; ?>" class="modal fade" role="dialog">
    <span class="btn-closemodal" data-dismiss="modal">&times;</span>
    <div class="modal-dialog modal-dialog-top modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h2>{{$video->title}}</h2>
          </div>
          <div class="modal-body">
         <iframe  id="iframe" src="{{ $video->url }}" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
          </div>
          <div class="modal-footer">
          <div class="download-files">
            <a href="{{ $video->file }}" target="_blank"><img src="{{asset('images/download.png')}}" alt="{{$video->title}}" width="5%">Скачать файлы</a>
          </div>
        </div>
        </div>
        </div>
      </div>
      <script type="text/javascript">
      $(".modal").on('hidden.bs.modal', function (e) {
        var div = document.getElementById("myModal<?php echo $count; ?>");
            var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
            iframe.postMessage('{"method":"pause"}', '*');});
      </script>
</div>
<?php $count++; ?>
@endforeach


</div>
</div>
@endsection
<script src='https://vjs.zencdn.net/7.6.0/video.js'></script>
