@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page" id="page-kursy">
  <div class="link">  
    <span><a href="/">Главная</a> / </span><span><a href="/courses">Курсы</a> / </span><span><a href="#">{{$category->name}}</a></span>
  </div>
  <div class="title">
    <h1>{{$category->name}}</h1>
  </div>
  <div class="breadcrumbs">
  @foreach($category->cate as $c)
    <a href="{{url('/')}}/category/{{$category->id}}/{{$c->id}}">{{$c->name}}</a>
    @endforeach
  </div> 
  <div class="about-course-text">
  </div>
  <div class="row">
        @foreach($course as $coursesItem)
    <div class="col-sm-4">
      <div class="card shadow card-news">
          <a href="/courses/{{$coursesItem->id}}">

        <div class="card-img">
          <img src="{{ Voyager::image($coursesItem->image)}}" alt="" width="100%">
        </div>
        <div class="card-content">
          <a href="/courses/{{$coursesItem->id}}">
          <div class="card-title">
            <h3>
              {{ $coursesItem->title }}
              </h3>
          </div>
          <div class="card-date">
            {{$coursesItem->smdescription}}
          </div>
          </a>
        </div>
      </div>
    </div>
    @endforeach
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#courseslink").addClass("active");
</script>
@endsection
