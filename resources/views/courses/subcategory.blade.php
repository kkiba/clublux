@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page" id="page-kursy">
  <div class="link">
    <span><a href="/">Главная</a> / </span><span><a href="/courses">Курсы</a> / </span><span><a href="#"> </a></span>
  </div>
  <div class="title">
    <h1><?php 
          echo $subcategory->name
        ?>
    </h1>
  </div>
  <div class="about-course-text">
    <p><?php
         echo $subcategory->description
        ?>
          </p>
  </div>
  <div class="row">

    @foreach($courses as $coursesItem)
      <div class="col-sm-4">
        
        <a href="/course/{{$coursesItem->id}}">
           <div class="card shadow card-news">
              <div class="card-img">
                <img src="{{ Voyager::image($coursesItem->image)}}" alt="" width="100%">
              </div>
              <div class="card-content">
                <div class="card-title">
                     <h3>{{$coursesItem->title }}</h3>
                </div>
                <div class="card-date">
                   {{$coursesItem->smdescription}}
               </div>
              </div>
          </div>
        </a>
    </div>
    @endforeach
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#courseslink").addClass("active");
</script>
@endsection
