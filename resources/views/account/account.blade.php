@extends('layouts.header')

@section('content')
<div class="col-sm-12 content-page tabcontent" id="page-account">
  <div class="link">
    <span><a href="/">Главная</a> / </span><span>Личный кабинет</span>
  </div>
  <div class="title">
    <h1>Личный кабинет</h1>
  </div>
  <div class="account-details">
    <div class="left-details col-sm-4">
      <div class="card white-bg img-name d-flex">
        <img class="rounded-circle" src="{{asset('images/img.jpg')}}" alt="" width="25%" height="80px">
        <div class="account-name">
          <p class="account-name-text">{{ Auth::user()->name}}</p>
          <p>{{ Auth::user()->email}}</p>
        </div>
  </div>
    </div>
    <div class="right-details col-sm-8">
      <div class="card white-bg personal-details">
        <div class="title border-bottom">
          <h3>Личный кабинет</h3>
        </div>
        <div class="mydetails">
          <div class="setting-title">
            <p id="collaps" data-toggle="collapse" data-target="#demo">Мои данные <span><i class="fa fa-caret-down"></i></span></p>
          </div>
          <div id="demo" class="collapse collapse-text">

            <form name="user-data" method="post" action="{{route('update.user')}}">
              @method('PUT')
              {{ csrf_field() }}
              <div class="profile-details">
                <div class="profile-name">
                  <label for="name">Имя</label>
                  <input type="text" name="name" value="{{ $user->name}}">
                </div>
                <div class="profile-surname">
                  <label for="surname">Фамилия</label>
                  <input type="text" name="surname" value="{{ $user->surname}}">
                </div>
                <div class="profile-fname">
                  <label for="fname">Отчество</label>
                  <input type="text" name="fname" value="{{ $user->fname}}">
                </div>
                <div class="profile-phone">
                  <label for="phone">Телефон</label>
                    <input type="text" name="phone" value="{{ $user->phone}}">
                </div>
              </div>
                  <div class="profile-save-button">
                      <div class="tabs-text">
                          <button class="btn btn-success btn2" type="submit" value="Edit">Сохранить</button>
                          <a class="btn btn-primary" href="#" onclick="$('#exampleModal-pasword').modal('show')">Сменить пароль</a>
                      </div>
                  </div>

          </form>
          </div>
        </div>
        <div class="history-course">
          <div class="setting-title">
            <p id="collaps" data-toggle="collapse" data-target="#demo2">История <span><i class="fa fa-caret-down"></i></span></p>
          </div>
            <div id="demo2" class="collapse collapse-text">
        </div>
      </div>

      </div>
    </div>

  </div>
</div>


<!--************************************************************* MODAL WINDOW PASSWORD ************************************************************-->
<div class="modal fade" id="exampleModal-pasword" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Сменить пароль</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form name="change-password" action="{{route('password.user')}}" method="post">
                          @method('PUT')
                          {{ csrf_field() }}
                            <div class="modal-text">
                                <p></p>
                            </div>
                            <input type="password" name="password_old" placeholder="Старый пароль">
                            <ul id="passwordOldErrors"></ul>
                            <input type="password" name="password" placeholder="Новый пароль">
                            <ul id="passwordNewErrors"></ul>
                            <input type="password" name="password_confirmation" placeholder="Подтвердите новый пароль">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="modal-footer">
                                <button id="update-password" type="submit" class="btn btn-success" value="edit">Сохранить</button>
                                <a href="#">Забыли пароль?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @if(!empty(Session::get('succes')) && Session::get('succes') == 13)
        <script>
        $(function() {
            $('#modal-info-changed').modal('show');
        });
        </script>
        @endif
        @if(!empty(Session::get('succes')) && Session::get('succes') == 3)
        <script>
        $(function() {
            $('#modal-password-changed').modal('show');
        });
        </script>
        @endif
        @if(!empty(Session::get('not')) && Session::get('not') == 422)
        <script>
        $(function() {
            $('#modal-password-notchanged').modal('show');
        });
        </script>
        @endif
        @if(!empty(Session::get('not')) && Session::get('not') == 2)
        <script>
        $(function() {
            $('#modal-password-confirmed').modal('show');
        });
        </script>
        @endif

<!--************************************************************* END MODAL WINDOW PASSWORD ************************************************************-->
<!-- МОДАЛ КОНТЕНТ -->
<div class="modal fade" id="modal-password-changed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Пароль был успешно изменен</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-success btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-password-notchanged" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Вы ввели неверный текущий пароль</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-danger btn-modal-1" data-dismiss="modal">Ввести занова</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-password-confirmed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Вы ввели неверный текущий пароль</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-danger btn-modal-1" data-dismiss="modal">Ввести занова</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-info-changed" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Данные были успешно изменены</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-success btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

window.FontAwesomeConfig = { autoReplaceSvg: false }

$('.setting-title').on('click', fav);
function fav(e) {
  $(this).find('i').toggleClass('fa-caret-down fa-caret-up');
}
</script>
@endsection
