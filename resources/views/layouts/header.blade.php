<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Lux Life</title>
	    <link rel = "icon" type = "image/png" href = "{{asset('images/iconfinder_laravel_1006880.png')}}">
		<link rel = "apple-touch-icon" type = "image/png" href = "{{asset('images/iconfinder_laravel_1006880.png')}}"/>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Our Custom CSS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.3/animate.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/modal-video@2.4.2/css/modal-video.min.css">
    <script src="{{asset('js/jssor.slider-27.5.0.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <link href="https://vjs.zencdn.net/7.6.0/video-js.css" rel="stylesheet">


 <!-- If you'd like to support IE8 (for Video.js versions prior to v7) -->
 <script src="https://vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
</head>
<body>
      <div class="wrapper">
        <nav id="sidebar" class="active">
            <div class="sidebar-header">
              <img src="{{asset('images/iconfinder_laravel_1006880.png')}}" alt="">
            </div>
            <ul class="list-unstyled components">
                <li id="defaultOpen" class="tablinks new" onclick="openCity(event, 'news')">
                    <a href="/"><img src="{{asset('images/iconfinder_news_115708.png')}}" alt="">Новости</a>
                </li>
                <li id="courseslink" class="tablinks" onclick="openCity(event, 'course')">
                    <a href="/courses"><img src="{{asset('images/iconfinder_education-degree-course-university-college_2205242.png')}}" alt="">Курсы</a>
                </li>
                <li class="tablinks" onclick="openCity(event, 'mclass')">
                    <a href="masterclass"><img src="{{asset('images/iconfinder_LIBRARY_2_753918.png')}}" alt="">Мастер Класс</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target="#modalcode"><img src="{{asset('images/iconfinder_power-button_353434.png')}}" alt="">Активация</a>
                </li>
                <li>
                    <a href="#" data-toggle="modal" data-target="#modalsupport"><img src="{{asset('images/iconfinder_headset_172475.png')}}" alt="">Поддержка</a>
                </li>
            </ul>
        </nav>
          <div class="modal fade" id="modalcode">
            <div class="modal-dialog modal-dialog-top">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Активация кода</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
              <form action="{{route('activate')}}" method="POST" >
                {{ csrf_field() }}
                <div class="modal-body">
                  <input id="codeInput" type="text" name="codeInput" value="">
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success" id="activateBtn">Активировать</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
              </form>
              </div>
            </div>
          </div>
          <div class="modal fade" id="modalsupport">
            <div class="modal-dialog modal-dialog-top">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Сообщить об ошибке</h4>
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
              <form action="{{route('support')}}" method="POST" >
                {{ csrf_field() }}
                <div class="modal-body">
                    <textarea id="message" name="message" placeholder="Написать..."></textarea>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success" id="sendmessage">Отпрвить</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                </div>
              </form>
              </div>
            </div>
          </div>
        <!-- Page Content Holder -->
        <div id="content">
          <div class="nav-full-screen">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <button type="button" id="sidebarCollapse" class="navbar-btn active">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav ml-auto menu-right">
                            <li class="nav-item">
                                <a class="nav-link language-flag" href="#">
                                  <img src="{{asset('images/russia-flag-icon-32.png')}}" alt="">
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                  <img src="{{asset('images/520567-32.png')}}" alt="">
                                </a>
                            </li>
                            <li class="nav-item">
                              <a id="navbarDropdown" class="nav-link dropdown-toggle dropdown-toggle2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                  {{ Auth::user()->name }}
                              </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                  <a class="dropdown-item" href="{{ asset('account')}}" >{{ __('Аккаунт') }}</a>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Выйти') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">{{Auth::user()->amount->amount}} $</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        </div>
        @if(!empty(Session::get('success')) && Session::get('success') == 27)
<script>
$(function() {
    $('#modal-succes').modal('show');
});
</script>
@endif
<div class="modal fade" id="modal-succes" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-top" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5>
                Ваш запрос принят в обработку</h5>
            </div>
            <div class="modal-btn">
                <button type="button" class="btn btn-success btn-modal-1" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
        @yield('content')
          </div>
        </div>
        <footer class="footer">
        <div class="container">
        <div class="col-sm-12">
          <p>Разработана в <a href="#">domalaq</a>  </p>
        </div>
        </div>
        </footer>
        <!-- jQuery CDN - Slim version (=without AJAX) -->
        <!-- Popper.JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{asset('js/main.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.4/js/lightgallery-all.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function () {
              $('#sidebarCollapse').on('click', function () {
                  $('#sidebar').toggleClass('active');
                  $(this).toggleClass('active');
              });
          });

        </script>

      </body>
        </html>
