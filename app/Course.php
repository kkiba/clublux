<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\CanTranslateCollection;
use TCG\Voyager\Traits\Translatable;

class Course extends Model
{
  use Translatable, CanTranslateCollection;

  protected $translatable = ['title', 'description', 'image'];

  protected $table = 'courses';

  protected $fillable = [
      'description',
      'image',
      'title'
  ];


  public function videos()
  {
    return $this->hasMany('App\Video');
  }
  public function comments()
  {
    return $this->hasMany(Comment::class);
  }
}
