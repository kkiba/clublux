<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Promocode;

class UserAmount extends Model
{
    protected $table = 'user_amount';
    protected $fillable = [
        'course_id',
        'user_id',
        'amount',
    ];
    public function promocode()
    {
        return $this->belongsTo(Promocode::class);
    }

  }
