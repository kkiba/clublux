<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Video extends Model
{
    public function courses()
    {
      return $this->belongsTo('App\Course');
    }
}
