<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Support extends Model
{
    protected $table = 'support';

    protected $fillable = [
        'user_id',
        'message',
        'user_email',
        'user_login',
    ];
}
