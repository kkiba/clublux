<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\CanTranslateCollection;

use TCG\Voyager\Traits\Translatable;

class Categoriesofcourse extends Model
{
  use Translatable, CanTranslateCollection;

  protected $translatable = ['name', 'image', 'aboutcourse'];

  protected $table = 'categoriesofcourses';
  

  protected $fillable = [
    'name',
    'image',
    'aboutcourse',
  ];
}
