<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CanTranslateCollection;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{
    use Translatable, CanTranslateCollection;

  protected $translatable = ['name', 'image'];

  protected $table = 'categories';
  

  protected $fillable = [
    'name',
    'image',

  ];
    public function cate()
    {
        return $this->hasMany(Category::class,'parent_id');
    }

}
