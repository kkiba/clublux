<?php

namespace App\Policies;

use App\Order;
use App\User;
use App\Course;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function show(User $user, $id="")
    {
      
      $course = Course::find($id);
      if(!$course)
      {
        return abort(404);
      }
      return Order::where('user_id', $user->id)->where('course_id', $course->id)->exists();
    }
}
