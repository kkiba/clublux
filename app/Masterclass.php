<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CanTranslateCollection;
use TCG\Voyager\Traits\Translatable;

class Masterclass extends Model
{
    
    use  Translatable, CanTranslateCollection;

    protected $translatable = ['name', 'description', 'image',];

    protected $table = 'masterclass';

    protected $fillable = [
        'description',
        'image',
        'name',
    ];
}
