<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function isConfirmed()
     {
         return !! $this->is_confirmed;
     }
     /**
      * Get email confirmation token
      * @return string
      */
     public function getEmailConfirmationToken()
     {
         $this->update([
             'confirmation_token' => $token = Str::random(),
         ]);
         return $token;
     }
     /**
      * Confirm user's email address
      * @return App\User
      */
     public function confirm()
     {
         $this->update([
             'is_confirmed' => true,
             'confirmation_token' => null,
         ]);
         return $this;
     }

     public function promocode()
     {
         return $this->hasOne(\App\Promocode::class);
     }

     public function addAmount()
     {
         return $this->promocode->balance;
     }
     

     public function amount() {
         return $this->hasOne(\App\UserAmount::class);
     }


     public function balance() {
        return $this->amount->amount;
     }


     public function addBalance($balance) {
        $this->amount->fill([
          'amount' => $this->amount->amount + $balance
        ])->save();

        return $this->amount->amount;
     }

     public function checkAvailability($balance) {
       if($this->amount->amount - $balance > 0) {
          return true;
       }
       return false;
     }

     public function removeBalance($balance) {
        if($this->checkAvailability($balance)) {
          $this->amount->fill([
            'amount' => $this->amount->amount - $balance
          ])->save();
        }
        return $this->amount->amount;
     }
 }
