<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\CanTranslateCollection;

use TCG\Voyager\Traits\Translatable;

class Subcategory extends Model
{
    use Translatable, CanTranslateCollection;

    protected $translatable = ['name', 'image', 'description'];
  
    protected $table = 'subcategories';
    
  
    protected $fillable = [
      'name',
      'image',
      'description',
    ];
}
