<?php

namespace App\Http\Controllers;

use App\News;
use App\Locale\Locale;
use Illuminate\Http\Request;

class NewsController extends Controller
{
  public function index(News $news) {

		$news = $news
			->translateCollection(
				$news
					->get()
			);

		return view('news.index', compact('news'));
	}
  public function show($id, $title="") {

      $news = News::find($id);
      if(!$news) {
        return abort(404);
      }
      $previous = News::previousItem($news->id);
      $next = News::nextItem($news->id);

      $previous = $previous ? $previous->translate(Locale::getLocale()) : null;
      $next = $next ? $next->translate(Locale::getLocale()) : null;

      return view('news.show', compact('news', 'previous', 'next'));
  }


}
