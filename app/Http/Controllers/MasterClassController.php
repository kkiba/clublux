<?php

namespace App\Http\Controllers;
use App\Masterclass;
use Illuminate\Http\Request;

class MasterClassController extends Controller
{
    public function index(Masterclass $masterclass){

        $masterclass = $masterclass->translateCollection(
				$masterclass
					->get()
			);
        return view('masterclass.index', compact('masterclass'));
    }
    public function show($id) 
    {
        
      $masterclass = Masterclass::find($id);

      if(!$masterclass) {
        return abort(404);
      }

        return view('masterclass.show', compact('masterclass'));

    }
}
