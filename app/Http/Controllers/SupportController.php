<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Support;

class SupportController extends Controller
{
    public function index()
    {

        $support = new Support();

        $support->message = request('message');
        $support->user_id = Auth::user()->id;
        $support->user_email = Auth::user()->email;
        $support->user_login = Auth::user()->name;

        $support->save();

        return back()->with('success', 27);
    }}
