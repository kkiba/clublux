<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Course;

class CategoryController extends Controller
{
    public function all() 
    {
        
        $categories=Category::where('parent_id', null)->get();
        return view('courses.index', compact('categories') );
    }

    public function index(Category $category)

    {

        $childrens = $category->cate->pluck('id')->toArray();
        $childrens[] = $category->id;
        $course = Course::whereIn('category_id', $childrens)->get();
        
        return view('courses.category', compact('course', 'category'));
    }


    public function show(Category $category, Category $subcategory)
    {
        if($subcategory->parent_id == $category->id)
        {
            $course = Course::where('category_id', $subcategory->id)->get();
            $category = $subcategory;
            return view('courses.category', compact('course', 'category'));
        }
        return abort(404);
    }
}
