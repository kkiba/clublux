<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Order;
use App\Course;

class CabinetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
      {
          $orders = Order::where('user_id', \Auth::user()->id)->get();
          $courses = [];
          foreach ($orders as $order) {
            $course = Course::find($order->course_id);
            array_push($courses, $course);
          }
          $user = $request->user();
          return view('account/account', compact('user', 'orders', 'courses'));
      }

      public function update(Request $request) {
          $this->validate($request, [
              'name' => 'required',
              'surname' => 'required',
              'fname' => 'required',
              'phone' => 'required',
          ]);

          $request->user()
              ->fill([
                  'name' => $request->name,
                  'surname' => $request->surname,
                  'fname' => $request->fname,
                  'phone' => $request->phone,
              ])->save();

          return back()->with('succes', 13);
              }
        public function password(Request $request)
              {
                $this->validate($request, [
                  'password_old' => 'required',
                  'password' => ['required', 'string', 'min:8', 'confirmed'],
                ]);
                if(!Hash::check($request->password_old, $request->user()->password)){
           return back()->with('not', 422);
       }

       $request->user()->fill([
           'password' => Hash::make($request->password)
       ])->save();


       return back()->with('succes', 3);
      }
}
