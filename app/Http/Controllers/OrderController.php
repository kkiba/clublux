<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Categoriesofcourse;
use App\Video;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\User;
use Illuminate\Support\Facades\Gate;



class OrderController extends Controller
{

    public function index($id, Course $course)
    {

        $course = Course::find($id);
        $videos = $course->videos;
        $categoriesofcourses = Categoriesofcourse::find($id);

        return view('courses.boughtcourse', compact('course', 'categoriesofcourses', 'videos'));

    }

    public function buy(Course $course)
    {

      if(!Order::where('user_id',  Auth::user()->id)->where('course_id',  $course->id)->exists()) {
                if(Auth::user()->checkAvailability($course->price)) {

                  Order::create([
                    'course_id'=> $course->id,
                    'user_id'=>Auth::user()->id
                  ]);

                  Auth::user()->removeBalance($course->price);
                  return back()->with('success', 1);
                }
                return back()->with('error', 2);
              }
      return back()->with(['errors' => 'Курс уже был куплен ранее']);

    }

}
