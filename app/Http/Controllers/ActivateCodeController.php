<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Promocode;

class ActivateCodeController extends Controller
{
    public function activate()
    {

        $addAmount = new Promocode();

        $addAmount->codeInput = request('codeInput');
        $addAmount->user_id = Auth::user()->id;
        $addAmount->balance = 100;
        $addAmount->save();

        return redirect('/courses');
    }
}
