<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\Comment;
use App\Categoriesofcourse;
use App\Categories;
use App\Video;
use App\Order;

class CourseController extends Controller
{

  public function show($id)
  {
    $courses = Course::find($id);
    $videos = $courses->videos;
    $categoriesofcourses = Categoriesofcourse::find($id);

    $purchase = Order::where('user_id',  Auth::user()->id)->where('course_id',  $courses->id)->exists();

    return view('courses.show', compact('courses', 'categoriesofcourses', 'videos', 'purchase', 'category'));

  }
  public function addComment(Request $request, Course $course)
  {
    $data = [
      'user_id' => $request->user()->id,
      'body' => $request->body
    ];

      $course->comments()->create($data);
      return back();

  }

}
