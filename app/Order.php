<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
  protected $table = 'orders';
  protected $fillable = [
      'course_id',
      'user_id',
  ];

}
