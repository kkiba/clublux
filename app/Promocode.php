<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Promocode extends Model
{
    protected $table = 'promocode';

    protected $fillable = [
        'user_id',
        'codeInput',
        'balance',
    ];
}
