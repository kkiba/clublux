
$('.modal').on('shown.bs.modal', function() {
  $(this).before($('.modal-backdrop'));
  $(this).css("z-index", parseInt($('.modal-backdrop').css('z-index')) + 1);
});
// Video Modal
jQuery(document).ready(function($) {
  $modal = $('#home-modal');
  var modal = document.getElementById('home-modal');
  var videoSrc = $("#home-modal source").attr("src");

  $('#home-play').on('click',function(e) {
    e.preventDefault();
    $modal.show();
    $('#my-video').attr("src", videoSrc+"?&amp;autoplay=1");
  });

  $('#close').on('click',function(e) {
    e.preventDefault();
    $("#home-modal source").attr("src", null);
    $modal.hide();
  });

  window.onclick = function(event) {
    if (event.target == modal) {
      $("#home-modal source").attr("src", null);
      $modal.hide();
    }
  }
});
