<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes(['verify' => true]);

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');


 Route::group(['middleware' => ['auth'/*, 'verified'*/ ]], function () {
 Route::get('/', function(){
     return view('news.index.page');
  });


  Route::resource('/', 'NewsController')
      ->only(['index'])
      ->names([
          'index' => 'news.index.page',
          'show' => 'news.show.page'
        ]);



  Route::get('/news/{id}/{title?}', 'NewsController@show');

  Route::get('/category/{category}', 'CategoryController@index')->name('category');
  Route::get('/category/{category}/{subcategory}', 'CategoryController@show')->name('subcategory');
  
  
  Route::get('courses', 'CategoryController@all')->name('course.index.page');
  
  Route::get('courses/{id}', 'CourseController@show')->name('course.category.page');
  
  Route::get('courses/subcategory/{id}', 'CourseController@subcategory')->name('subcategory');

  Route::get('course/{id?}', 'CourseController@show')->name('course.show.page');

  Route::post('course/{course}/comments', 'CourseController@addComment');
  Route::get('masterclass', 'MasterClassController@index')->name('masterclass');
  Route::get('masterclass/{id}', 'MasterClassController@show')->name('submaster');

  Route::post('support', 'SupportController@index')->name('support');



  Route::post('activate', 'ActivateCodeController@activate')->name('activate');

  Route::get('page/{id}', 'OrderController@index')->name('boughtcourse')
    ->middleware('can:show, App\Order,id');

    Route::post('course/{course}/buy', 'OrderController@buy')->name('buy');

  Route::prefix('account')->group(function(){

  Route::get('/', 'CabinetController@index')->name('account');

  Route::put('/account/user/update', 'CabinetController@update')->name('update.user');

  Route::put('/account/user/password', 'CabinetController@password')->name('password.user');
          });


});
Route::get('/home', 'HomeController@index')->name('home');
